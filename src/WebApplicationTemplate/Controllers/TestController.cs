﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationTemplate.Controllers
{
    public class TestController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to CICD automated pipeline";
            return View();
        }
    }
}