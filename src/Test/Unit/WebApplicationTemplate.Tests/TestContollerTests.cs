﻿using System;
using System.Linq;
using System.Web.Mvc;
using NUnit.Framework;
using Webapplicationtemplate;
using WebApplicationTemplate.Controllers;
using Microsoft.CSharp;
using FluentAssertions;
//using System.Core;

namespace Webapplicationtemplate
{
    public class Testcontroller
    {
        [Test]
        public void Index()
        {
            TestController controller = new TestController();
            
            ViewResult result = controller.Index() as ViewResult;
            
            result.Should().NotBeNull();
        }

        [Test]
        public void Index_verifyMessage()
        {
            var displayMessage = "Welcome to CICD automated pipeline";
            
            TestController controller = new TestController();
            
            ViewResult result = controller.Index() as ViewResult;

            result.ViewData.Values.First().ShouldBeEquivalentTo(displayMessage);
        }
    }
}
