﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSpec;
using System.Web.Mvc;
using FluentAssertions;
using WebApplicationTemplate.Controllers;

namespace WebapplicationTemplateTest
{
    public class WebapplicationTemplate : nspec
    {
        TestController _testController;
        void before_each()
        {
            _testController = new TestController();
        }
        void Given_webapplication_is_launched()
        {
            string displayMessage = "Welcome to CICD automated pipeline";
            
            ViewResult result=null;
            act = () =>
            {
                result = _testController.Index() as ViewResult;
            };

            it["Should return message"] = () => result.ViewData.Values.First().ShouldBeEquivalentTo(displayMessage);
            it["Should not be null"] = () => result.Should().NotBeNull();
        }
    }
}
