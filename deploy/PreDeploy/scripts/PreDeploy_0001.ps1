param(  $baseDir = (Resolve-Path .\).Path, `
        $azureRestLibDir = (Resolve-Path "$baseDir\lib").Path, `
        $adalLibDir = (Resolve-Path "$baseDir\adal").Path, `
        [ValidateNotNullOrEmpty()]$tenantName = $(throw "TenantName empty"), `
		[ValidateNotNullOrEmpty()]$deploymentUserName = $(throw "deploymentUserName empty"), `
        [ValidateNotNullOrEmpty()]$deploymentPassword = $(throw "deploymentPassword empty"), `
        [ValidateNotNullOrEmpty()]$azureSubscriptionId = $(throw "azureSubscriptionId empty"), `
        [ValidateNotNullOrEmpty()]$adalAdTenantId = $(throw "adalAdTenantId empty"), `
        [ValidateNotNullOrEmpty()]$dataCenter = $(throw "dataCenter empty"))

$ErrorActionPreference = "stop"

. "$azureRestLibDir\azure_management.ps1" $script:customLibDir $azureRestLibDir
. "$azureRestLibDir\azure_rest_client.ps1" $azureRestLibDir
. "$azureRestLibDir\adal_authentication_handler.ps1" $adalLibDir $azureRestLibDir
. "$azureRestLibDir\azure_ad_rest_client.ps1" $azureRestLibDir $adalLibDir
. "$azureRestLibDir\storage_container_client.ps1"

    $userName = $deploymentUserName
    $securePassword = ConvertTo-SecureString -String $deploymentPassword -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential($userName, $securePassword)
    
    Login-AzureRmAccount -Credential $cred
    Select-AzureRmSubscription -SubscriptionId $azureSubscriptionId

	$script:azureManagement = new_azure_management -SubscriptionId $azureSubscriptionId -AzCopyDir "" -AdalAdTenantId $adalAdTenantId -LoginHint $deploymentUserName -Password $deploymentPassword -Thumbprint $thumbprint
									

function deploy {

	
    $deploymentStartTime = Get-Date

    Write-Host "Deploy Azure Resources Started"

    try {
        
        $resourceGroup = "$($tenantName)resourcegroup" 
        $storageAccountName = "$($tenantName)fu"
        $websiteName = "$($tenantName)template"

        Write-Host "TenantName: $tenantName"
        Write-Host "ResourceGroup: $resourceGroup"
        Write-Host "StorageAccountName: $storageAccountName"
        Write-Host "WebsiteName: $websiteName"

        create_resource_group -resourceGroupName $resourceGroup -dataCenter $dataCenter

        deploy_arm_template -resourceGroup $resourceGroup `
                    -storageAccountName $storageAccountName `
                    -websiteName $websiteName
    

        $timeTakenForDeployment = (Get-Date) - ($deploymentStartTime)
        write-host "************Deployment success after running for (in mins) :" + $([math]::abs($timeTakenForDeployment.TotalMinutes)) + "***********" -ForegroundColor Green
    }
    catch {
        $timeTakenForDeployment = (Get-Date) - ($deploymentStartTime)
        write-host "************Deployment failed after running for (in mins) :" + $([math]::abs($timeTakenForDeployment.TotalMinutes)) + "***********" -ForegroundColor Red

		write-host "Completed Tasks Status : "  -ForegroundColor Green
        $script:LastKnownStatus |% { $_ }
		write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
		write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
        throw
    }
}



function create_resource_group($resourceGroupName, $dataCenter) {
    $script:azureManagement.CreateResourceGroupIfNotExists($resourceGroupName, $dataCenter);
}

function deploy_arm_template($resourceGroup, $websiteName ){

    $templateFile = "$baseDir\WebAppCreationArmTemplate.json"
    $hostingplanName = "appplan_$websiteName"

    Write-Host "Deploying azure resources through ARM template"
    Write-Host "ResourceGroup Name: $resourceGroup"
    Write-Host "TemplateFile: $templateFile"
    Write-Host "WebSite Name: $websiteName"
    Write-Host "HostingPlan Name: $hostingplanName"

    New-AzureRmResourceGroupDeployment -ResourceGroupName $resourceGroup `
                    -TemplateFile $templateFile `
                    -Name "ResourceGroupDeployment" `
                    -hostingPlanName $hostingplanName `
                    -skuName "S3" `
                    -skuCapacity 2 `
                    -webSiteName $websiteName

}




