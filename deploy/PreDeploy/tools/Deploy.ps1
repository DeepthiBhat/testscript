param($installPath, $toolsPath, $package, $project)

. .\scripts\PreDeploy_0001.ps1 -baseDir .\scripts 	`
	-deploymentUserName $deploymentUserName         `
	-deploymentPassword $deploymentPassword			`
	-azureSubscriptionId $azureSubscriptionId		`
	-tenantName $tenantName							`
	-adalAdTenantId $adalAdTenantId					`
	-datacenter $datacenter
deploy
