# ************************************************************************
# ************************************************************************
# *****      COPYRIGHT 2014 - 2015 HONEYWELL INTERNATIONAL SARL      *****
# ************************************************************************
param(	[ValidateNotNullOrEmpty()]$tenantName = $(throw "tenantName empty"),	`
[ValidateNotNullOrEmpty()]$deploymentUserName = $(throw "deploymentUserName empty"), `
        [ValidateNotNullOrEmpty()]$deploymentPassword = $(throw "deploymentPassword empty"), `
        [ValidateNotNullOrEmpty()]$azureSubscriptionId = $(throw "azureSubscriptionId empty"))

$ErrorActionPreference = "stop"

[Reflection.Assembly]::LoadWithPartialName("System.Web.Extensions")
Add-Type -Assembly System.Web

$script:LastKnownStatus = @()

#Predeploy
function _log_start ($name, $params) {
	$date = Get-Date -format s
	$quotedParams = ""
	if ($params) {
		$paramsArray = $params.split(",") | ForEach { """"+"$_".trim()+"""" }
		$quotedParams = $paramsArray -join " "
	}
	write-host $date "started" $name $quotedParams  -ForegroundColor Green
}

#Predeploy
function _log_finish ($name) {
	$date = Get-Date -format s
	write-host $date "finished" $name -ForegroundColor Green
}

#Predeploy
function deploy {
	$userName = $deploymentUserName
    $securePassword = ConvertTo-SecureString -String $deploymentPassword -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential($userName, $securePassword)
    
    Login-AzureRmAccount -Credential $cred
    Select-AzureRmSubscription -SubscriptionId $azureSubscriptionId
	
	_log_start $MyInvocation.MyCommand.Name
    $deploymentStartTime = Get-Date
	
	try
	{
	    _startwebapplicationIfStopped $tenantName
        write-host "************Total Time Taken For Deployment(in mins) :" + $([math]::abs($timeTakenForDeployment.TotalMinutes)) + "***********" -ForegroundColor Blue
	}
	catch
	{
        $timeTakenForDeployment = (Get-Date) - ($deploymentStartTime)
         write-host "************Deployment failed after running for (in mins) :" + $([math]::abs($timeTakenForDeployment.TotalMinutes)) + "***********" -ForegroundColor Red
		write-host "Completed Tasks Status : "  -ForegroundColor Green
        $script:LastKnownStatus |% { $_ }
		write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
		write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
    }

	_log_finish $MyInvocation.MyCommand.Name
}

function _startwebapplicationIfStopped($tenantName)
{
   $webApplicationName = $tenantName+ 'template'
   write-host "Checking status:" $webApplicationName
   $state = (Get-AzureRmWebApp -Name $webApplicationName).State
   write-host "State of "  $webApplicationName ":" $state
   if($state -eq "Stopped" -Or $state -eq "" -Or $state -eq $null)
   {
      write-host "Starting" $webApplicationName
      Start-AzureRmWebApp $webApplicationName
	  write-host "started $webApplicationName"
   }   
}
