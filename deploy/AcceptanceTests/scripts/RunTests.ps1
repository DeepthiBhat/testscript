# ************************************************************************
# *****      COPYRIGHT 2014 - 2015 HONEYWELL INTERNATIONAL SARL      *****
# ************************************************************************
param(	$baseDir = (Resolve-Path .\).Path
)
$ErrorActionPreference = "stop"

function _getFullPathFromRelativePath($relativePath){
	[System.IO.Path]::GetFullPath($relativePath)
}

function _addAATArtifact()
{
    $logsPath = _getFullPathFromRelativePath "$baseDir\tests\nspecrun.log"
	Write-Host "AAT Logs path is " $logsPath
	New-OctopusArtifact -Path "$logsPath" -Name "AAT_Logs.txt"
}

function RunTests{
    Write-Host "Running Post Tests"
    _addAATArtifact
}
