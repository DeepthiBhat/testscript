param($installPath, $toolsPath, $package, $project)

. .\scripts\RunTests.ps1 -baseDir .\scripts 	
	
try {

	& .\scripts\runtestscmd.cmd
	
	Write-Host "Tests ExitCode - " $LastExitCode
	$acceptableErrorCodes = $(0,1,2)
	if($acceptableErrorCodes.Contains($LastExitCode) -eq $false){
		Write-Host "Acceptance Test Failed.Returning error code 1"
		RunTests
		exit 1;
	}
	Write-Host "Acceptance Test Succeded"
	RunTests
	exit 0;
}
catch [Exception] {
	Write-Host "RunTests threw an exception: "
	Write-Host "$($_.Exception.Message)"
	Write-Host "$($_.InvocationInfo.PositionMessage)"

    exit 1;
}