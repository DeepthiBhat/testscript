
# ************************************************************************
# *****      COPYRIGHT 2014 - 2015 HONEYWELL INTERNATIONAL SARL      *****
# ************************************************************************
function showNotification($message,$level)
{
    
	[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
	$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon 
    
    if($level -eq $null)
    {
	    $objNotifyIcon.Icon = [System.Drawing.SystemIcons]::Information
	    $objNotifyIcon.BalloonTipIcon = "Info" 
    }
    
    elseif($level -eq "Error")
    {
        $objNotifyIcon.Icon = [System.Drawing.SystemIcons]::Error
	    $objNotifyIcon.BalloonTipIcon = "Error" 
    }
	$objNotifyIcon.BalloonTipText = $message
	$objNotifyIcon.BalloonTipTitle = "Bulldog"
	 
	$objNotifyIcon.Visible = $True 
	$objNotifyIcon.ShowBalloonTip(500000)
		
}
