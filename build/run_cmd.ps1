
# ************************************************************************
# *****      COPYRIGHT 2014 - 2015 HONEYWELL INTERNATIONAL SARL      *****
# ************************************************************************
function run_cmd ($commandName, $arguments, $acceptableExitCodes, $startInfoFunc) {
	Write-Host "Command Name: $commandName"
	Write-Host "Arguments: $arguments"

	$process = New-Object Diagnostics.Process 
	$startInfo = $process.StartInfo
	$startInfo.FileName = $commandName
	$startInfo.UseShellExecute = $false
	$startInfo.Arguments = $arguments
	$startInfo.WorkingDirectory = $script:binariesDirectory

	if($startInfoFunc -ne $null) {
		& $startInfoFunc $startInfo
	}

	$startInfo.RedirectStandardError = $true
	$startInfo.RedirectStandardOutput = $true
	$startInfo.RedirectStandardInput = $false

	$errEvent = Register-ObjectEvent -InputObj $process -Event "ErrorDataReceived" -Action { param([Object] $sender, [Diagnostics.DataReceivedEventArgs] $e)
		if ($e.Data) {
			Write-Host $e.Data
		}
		else {
			New-Event -SourceIdentifier "LastMsgReceived"
		}
	}

	$outEvent = Register-ObjectEvent -InputObj $process -Event "OutputDataReceived" -Action { param([Object] $sender, [Diagnostics.DataReceivedEventArgs] $e)
		Write-Host $e.Data
	}

	$exitCode = -1
	if ($process.Start()) {
		$process.BeginOutputReadLine()
		$process.BeginErrorReadLine()

		$process.WaitForExit()
		$exitCode = [int]$process.ExitCode
		Wait-Event -SourceIdentifier "LastMsgReceived" -Timeout 60 | Out-Null

		$process.CancelOutputRead()
		$process.CancelErrorRead()
		$process.Close()
	}

	if(!($acceptableExitCodes -Contains $exitCode)) { throw $exitCode }
}
