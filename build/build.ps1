# ************************************************************************
# *****      COPYRIGHT 2014 - 2015 HONEYWELL INTERNATIONAL SARL      *****
# ************************************************************************
$ErrorActionPreference = "Stop"

Framework 4.5.1

$buildUtilsVersion = "0.20.0"
$buildUtilsPackageName = "acscp.bulldog.build.powershell"
$arm_templates = "arm_templates"
$azurePowershellPackageName = "acscp.bulldog.azure.powershell"
$azurePowershellVersion = "30.8.0-ci0078"

$bulldogArtifactoryNugetServer = "https://artifactory.honeywell.com/artifactory/api/nuget/ConnectedBuildingsPlatform-Code"
Invoke-Expression -Command "..\src\.nuget\nuget.exe install $buildUtilsPackageName -version $buildUtilsVersion -OutputDirectory . -Source $bulldogArtifactoryNugetServer  -configFile ..\src\.nuget\nuget.config"
. .\$buildUtilsPackageName.$buildUtilsVersion\tools\bulldogCommonBuildScripts.ps1
. .\$buildUtilsPackageName.$buildUtilsVersion\tools\packageChecker.ps1
. .\$buildUtilsPackageName.$buildUtilsVersion\tools\common_assemblyinfo_checker.ps1
. .\$buildUtilsPackageName.$buildUtilsVersion\tools\java_swagger_client_scripts.ps1
properties {
	$rootDir = Resolve-Path ".."
	$buildDir = "$rootDir\build"
	$binariesDir = "$rootDir\binaries"
    $octopusPackageFolderDir = "$rootDir\binaries\octopus"
	$deployScriptsFolder = "$rootDir\deploy"
	$buildNumber = "0.0.0.0"
	$configuration = "debug"
	$script:tempPackageDirBase = "C:\Temp\WebApplicationTemplate"
	$script:tempPackageDir = "$($script:tempPackageDirBase)\0.0.0.0-ci0000" #Updated by getassemblyinfo
	$script:enableRemoteDebugging = $enableRemoteDebugging
	$script:srcRoot = "$rootDir\src"
	$script:solution = "$script:srcRoot"
	$script:solutionName = "WebApplicationTemplate"
    $script:unittests = "$script:srcRoot\Test\Unit"
    $script:acceptanceTests = "$script:srcRoot\Test\Acceptance"

	$script:packageStagingDir = "$binariesDir\package_staging"

	$new_staging_proj_def = { param($name, $parentDir)
		$obj = New-Object PSObject -Property @{
			Name = $name;
			OutDir = "$script:packageStagingDir\$name";
			SrcDir = "$parentDir\$name";
		}
		$obj
	}

	$new_proj_def = { param($name, $parentDir)
		$obj = New-Object PSObject -Property @{
			Name = $name;
			OutDir = "$binariesDir\$name";
			SrcDir = "$parentDir\$name";
		}
		$obj
	}

	$new_proj_def_named = { param($name, $parentDir, $projectDir)
		$obj = New-Object PSObject -Property @{
			Name = $name;
			OutDir = "$binariesDir\$name";
			SrcDir = "$parentDir\$projectDir";
		}
		$obj
	}

	$script:websiteProject = & $new_proj_def "WebapplicationTemplate" $script:solution

	$script:nuGetDir = "$script:solution\.nuget"
	$script:nugetCli = "$script:nugetDir\nuget.exe"
	$script:nugetConfigPath = "$script:nugetDir\NuGet.Config"

	$script:packagesDir = "$script:solution\packages"

	$gitVersionVersion = "3.5.4"
	$gitVersionPackageName = "GitVersion.CommandLine"
	$script:gitVersionExe = "$($script:packagesDir)\$gitVersionPackageName.$gitVersionVersion\tools\GitVersion.exe"

	$script:nugetVersionNumber = "0.0.0.0-ci0000"
	$script:informationVersion = "0.0.0.0"
	$script:assemblyVersion = "0.0.0.0"
	$script:gitBranch = ""

    $script:isMasterBranch = $false
	
	$octopusDeployServerIp = "SET_BY_BUILD_SYSTEM"	
	$octopusDeployApiKey = "SET_BY_BUILD_SYSTEM"
	$octoExe = "$($script:packagesDir)\OctopusTools.3.3.0\tools\octo.exe"

	$isBuildServer = $false

	$gitPath = "SET_BY_BUILD_SYSTEM"
}

task default -depends restore_packages, clean, `
check_commonAssembly_info, `
update_assembly_info, `
check_packages, `
quick_build, `
package_website, `
package_octopus_acceptance, `
package_octopus_predeploy, `
package_octopus_postdeploy, 
publish_nuget_package, `
create_octopus_release, `
post_build_clean

task quick_build -depends restore_packages, `
build_solution, `
run_tests

task clean {
	if(Test-Path $binariesDir) {
		Remove-Item $binariesDir -Force -Recurse
	}
	New-Item $binariesDir -Type Directory
  run_cmd $script:nugetCli @("restore", "$script:solution\$($script:solutionName).sln")
}

task check_packages{
$multipleVersionPacakgeList = analyzePackages
if($multipleVersionPacakgeList.Count -gt 0)
    {
		$multipleVersionPacakgeList
		throw "Multiple Package Version exists!!!"
    }
}

task package_website -depends build_solution{
	package_and_pack_website $script:websiteProject
}
task check_commonAssembly_info{
$projectsWithoutCommonAssemblyInfoLink = analyzeProjects
if($projectsWithoutCommonAssemblyInfoLink.Count -gt 0)
    {
		$projectsWithoutCommonAssemblyInfoLink
		throw "The projects above does not have link to CommonAssemblyInfo.cs"
    }
}

task post_build_clean {
	if((Test-Path $script:packageStagingDir)) {
		Remove-Item $script:packageStagingDir -Recurse -Force
	}
	if((Test-Path $script:tempPackageDir)) {
		Remove-Item $script:tempPackageDir -Recurse -Force
	}
}

task restore_packages {
    "$script:solution\WebApplicationTemplate.sln" | ForEach-Object {
        run_nuget @("restore", $_, "-configfile $script:nugetConfigPath")
    }
}

task update_assembly_info {
    update_assembly_info $gitVersionExe "$rootDir\CommonAssemblyInfo.cs"
}

task build_solution {
	build_solution $script:solution $script:solutionName "sln" "clean;build"
}

task run_tests {

	$unitTestProjects = Get-ChildItem $script:unittests -Recurse -Include *.tests.csproj -Exclude *.int.tests.csproj
	run_tests_in_parallel $unitTestProjects
}

function run_tests_in_parallel {param($testProjects)
	$folder = "$($buildDir)/BuildResults"
	ensureCleanNewFolder $folder
    
	$testJobs = New-Object System.Collections.ArrayList
    $testOutputFilePaths = New-Object System.Collections.ArrayList
    
    $scriptBlock = 
    {
        param ([string] $buildDir, 
        [string] $commandName, 
        [string] $commandArgs)
        
        . "$buildDir/run_cmd.ps1"
        run_cmd $commandName $commandArgs 0
    }
    
    foreach ($testProject in $testProjects) {
        $testOutputFullPath  = "$($folder)/testoutput-$($testProject.Name).txt"
        $testOutputFilePaths.Add($testOutputFullPath) | Out-Null
        
        $job = Start-Job -ScriptBlock $scriptBlock -ArgumentList $buildDir, "$script:srcRoot\packages\NUnit.ConsoleRunner.3.2.0\tools\nunit3-console.exe", "$($testProject.FullName) /out:$($testOutputFullPath) --domain=Multiple --result:$($folder)/$($testProject.BaseName).TestResult.xml;format=nunit3"
        
        $testJobs.Add($job) | Out-Null
	}
    
    while ($testJobs.Count -gt 0)
    {
        $job = Wait-Job $testJobs -Any
        
        if ($job.State -eq 'Failed')
        {
            foreach ($testJob in $testJobs)
            {
                Stop-Job -Job $testJob
                Receive-Job -Job $testJob  
                Remove-Job -Job $testJob
            }

            exit 1
        }
        
        Receive-Job -Job $job        
        Remove-Job -Job $job        
        $testJobs.Remove($job)
    }
    
	Write-Host "Deleting test output files"
	$testOutputFilePaths | % { Remove-Item $_ }
}

function package_and_pack_website { param($projDef)
	ensureFolder $octopusPackageFolderDir
	package_web_site $projDef

	$destination = "$script:tempPackageDir\$($projDef.Name)"
	ensureFolder "$destination\scripts"
	ensureFolder "$destination\tools"

	$nuspecFile = "$deployScriptsFolder\$($projDef.Name)\$($projDef.Name).nuspec"
	run_nuget @("pack", $nuspecFile, "-Version $script:nugetVersionNumber", "-OutputDirectory $octopusPackageFolderDir", "-BasePath $destination")
}

function package_web_site { param($projDef)
	$name = $projDef.Name
	$srcDir = $projDef.SrcDir
	build_project $srcDir $name "csproj" "package" "true"
}

function build_project ($sourceDirectory, $name, $extension, $target, $enablePackagingToTempDir = "false"){
	$tempDirParam = ""
    if($enablePackagingToTempDir -eq "true"){
		$tempDirParam = " /p:_PackageTempDir=$script:tempPackageDir/$name"
	}
	run_cmd "msbuild" "`"$sourceDirectory\$name.$extension`" /t:$target /p:Configuration=$configuration /p:VisualStudioVersion=14.0 /m $tempDirParam"
}

function build_solution ($sourceDirectory, $name, $extension, $target, $enablePackagingToTempDir = "false"){
	$tempDirParam = ""
    if($enablePackagingToTempDir -eq "true"){
		$tempDirParam = " /p:_PackageTempDir=$script:tempPackageDir"
	}
	run_cmd "msbuild" "`"$sourceDirectory\$name.$extension`" /t:$target /p:Configuration=$configuration /p:VisualStudioVersion=14.0 /m $tempDirParam"
}

task build_nuget_package {
	Remove-Item "$binariesDir\*.nupkg"
	build_all_nuget_packages_below_folder $script:solution
	build_all_nuget_packages_below_folder $script:acceptanceTests
}

function build_all_nuget_packages_below_folder($folder){
	Get-ChildItem $folder -Recurse -Include *.nuspec | ForEach-Object {
		$projFileName = $_ -replace ".nuspec",".csproj"
		run_nuget @("pack", "$projFileName", "-Version $script:nugetVersionNumber", "-OutputDirectory $binariesDir", "-Properties `"customVersion=$script:nugetVersionNumber`"")
	}
}

task publish_nuget_package -Depends build_nuget_package{
	$octopusDeployNuGetPublish = "http://$octopusDeployServerIp/nuget/packages?replace=true"
	publish_nuget_packages_in_folder $binariesDir $isBuildServer $script:isMasterBranch $bulldogArtifactoryNugetServer $bulldogUnstableArtifactoryNugetServer $bulldogArtifactoryApiKey
	if ($isBuildServer) {
		Get-ChildItem "$octopusPackageFolderDir\*.nupkg" | ForEach-Object {
			run_nuget @("push", "$($_.FullName)", "-Source $octopusDeployNuGetPublish", "-NonInteractive", "-apikey $octopusDeployApiKey")
		}
	}
}

task create_octopus_release -Depends publish_nuget_package {
	$octopusDeployApi = "https://$octopusDeployServerIp/"
	if ((Get-Command "git.exe" -ErrorAction SilentlyContinue) -eq $null) { 
		Write-Host "Cannot find git in environment, using passed in path: $gitPath"
		$latestCommit = & "$gitPath\git" log -1 | Out-String
	}
	else {
		$latestCommit = git log -1 | Out-String
	}

	$latestCommitWithoutQuotes = $latestCommit -replace """", "'"
    $currentgitversion = & $gitVersionExe | select-string "NuGetVersionV2"
    $currentgitversion = $currentgitversion -replace ",",""
    $currentgitversion = $currentgitversion -replace "NuGetVersionV2","GitVersion"
    $latestCommitWithoutQuotes = $latestCommitWithoutQuotes  + $currentgitversion
	Write-Host $latestCommitWithoutQuotes
	if ($isBuildServer){
		if ($script:isMasterBranch) {
			run_cmd $octoExe @("create-release", `
					"--project ""WebapplicationTemplate""", `
                     "--packageversion $($script:nugetVersionNumber)", `
					"--channel ""Master Branch""", `
					"--server $octopusDeployApi", `
					"--apiKey $octopusDeployApiKey", `
					"--debug", `
					"--ignoreSslErrors", `
					"--releasenotes ""$latestCommitWithoutQuotes""")
		}

        else {
			run_cmd $octoExe @("create-release", `
					"--project ""WebapplicationTemplate""", `
                     "--packageversion $($script:nugetVersionNumber)", `
					"--channel ""Feature Branch""", `
					"--server $octopusDeployApi", `
					"--apiKey $octopusDeployApiKey", `
					"--debug", `
					"--ignoreSslErrors", `
					"--releasenotes ""$latestCommitWithoutQuotes""")
		}
	}
}

task package_octopus_armdeploy{
	removeFolder $script:tempPackageDir\PreDeploy

	Copy-Item "$deployScriptsFolder\ArmTemplate" $script:tempPackageDir -Recurse
	$folder = "$script:tempPackageDir\ArmTemplate"
	
	run_nuget @("pack", "$folder\webapplicationtemplate.nuspec", "-Version $script:nugetVersionNumber", "-OutputDirectory $octopusPackageFolderDir")
}

task package_octopus_predeploy {
	removeFolder $script:tempPackageDir\predeploy

	Copy-Item "$deployScriptsFolder\PreDeploy" $script:tempPackageDir -Recurse
	$folder = "$script:tempPackageDir\PreDeploy"
	
	run_nuget @("restore", "$folder\packages.config", "-PackagesDirectory $folder\packages", "-configfile $script:nugetConfigPath")

	ensureCleanNewFolder $folder\scripts\adal
	copyFilesFromFolder "$folder\packages\Microsoft.IdentityModel.Clients.ActiveDirectory.2.19.208020213\lib\net45\*.dll" "$folder\scripts\adal"

	ensureCleanNewFolder $folder\scripts\lib
	copyFilesFromFolder "$folder\packages\$azurePowershellPackageName.$azurePowershellVersion\*.ps1" "$folder\scripts\lib"
	copyFilesFromFolder "$folder\packages\$azurePowershellPackageName.$azurePowershellVersion\$arm_templates\*.json" "$folder\scripts\lib"

	run_nuget @("pack", "$folder\webapplicationtemplate.predeploy.nuspec", "-Version $script:nugetVersionNumber", "-OutputDirectory $octopusPackageFolderDir")
}

task package_octopus_postdeploy {
	removeFolder $script:tempPackageDir\postdeploy
	Copy-Item "$deployScriptsFolder\PostDeploy" $script:tempPackageDir -Recurse
	$folder = "$script:tempPackageDir\PostDeploy"

	run_nuget @("pack", "$folder\webapplicationtemplate.postdeploy.nuspec", "-Version $script:nugetVersionNumber", "-OutputDirectory $octopusPackageFolderDir")
}

task package_octopus_acceptance {
    ensureFolder $octopusPackageFolderDir
	removeFolder $script:tempPackageDir\acceptancetests
	Copy-Item "$deployScriptsFolder\acceptancetests\" $script:tempPackageDir -Recurse
	$folder = "$script:tempPackageDir\acceptancetests"

	run_nuget @("restore", "$folder\packages.config", "-PackagesDirectory $folder\packages", "-configfile $script:nugetConfigPath")

	ensureCleanNewFolder $folder\scripts\nunit.runners
	Copy-Item "$folder\packages\NSpec.3.1.0\tools\net451\win7-x64\" "$folder\scripts\NSpec" -Recurse

	ensureCleanNewFolder $folder\scripts\tests
	copyFilesFromFolder "$script:acceptanceTests\webapplicationtemplate.Acceptance.Tests\bin\$configuration\*.*" "$folder\scripts\tests"

	run_nuget @("pack", "$folder\webapplicationtemplate.acceptancetests.nuspec", "-Version $script:nugetVersionNumber", "-OutputDirectory $octopusPackageFolderDir", "-Properties Configuration=$configuration")
}


